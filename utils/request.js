import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import cookie from "js-cookie";
// 创建axios实例
const service = axios.create({
    baseURL: process.env.NUXT_ENV.API_BASE_URL,
    // baseURL: 'http://localhost:8160',

    timeout: 5000, // 请求超时时间
    //全局设置 axios 发送请求带上cookie
    // withCredentials: true,
})
// http request 拦截器
service.interceptors.request.use(
    config => {
        let token = cookie.get("campus-token");;
        if (typeof token !== 'undefined') {
        config.headers['token'] = cookie.get("campus-token");
        // cookie.remove("campus-token") //删除
        }
        return config
    },
    err => {
        return Promise.reject(err)
    })
// http response 拦截器
service.interceptors.response.use(
    response => {
        if (response.data.code !== 200) {
            Message({
                message: response.data.message,
                type: 'error',
                duration: 5 * 1000
            })
            return Promise.reject(response.data)
        } else {
            return response.data
        }
    },
    error => {
        return Promise.reject(error.response)
    })
export default service