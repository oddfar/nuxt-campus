import request from '@/utils/request'

const api_name = `/tourist`

export default {
    //用户通过cid查询的数据
    getUserByCids(cid) {
        return request({
            url: `${api_name}/content/` + cid,
            method: `get`,
        });
    },
    // 获取评论
    getpinlun(cid) {
        return request({
            url: `${api_name}/getComment/` + cid,
            method: `get`,
        })
    },
    //获取所有分类
    getMetas() {
        return request({
            url: `${api_name}/getMetas`,
            method: `get`,
        })
    },
    //获取消息盒子内容
    getContent(contentVo) {
        return request({
            url: `${api_name}/contents`,
            method: `post`,
            data: contentVo
        })
    },
    //游客通过cid查询的数据
    getUserCid(cid) {
        return request({
            url: `${api_name}/contents`,
            method: `post`,
            data: cid
        });
    },
    //判断 邮箱或昵称 是否存在
    isExist(loginVo) {
        return request({
            url: `${api_name}/isExist`,
            method: `post`,
            data: loginVo
        });
    }
}