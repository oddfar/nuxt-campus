import request from '@/utils/request'

const api_name = `/api/operate`
export default {
    // 删除发表的评论
    deletePin(coid) {
        return request({
            url: `${api_name}/delComment`,
            method: `post`,
            data: coid
        })
    },
    // 删除发布的信息
    deleteCon(cid) {
        return request({
            url: `${api_name}/delContent`,
            method: `post`,
            data: cid
        })
    },
    //提交点赞信息
    getUserIsZan(cid) {
        return request({
            url: `${api_name}/zan/` + cid,
            method: `post`,
        })
    },
    // 发布评论
    PostAcomment(Pgroup) {
        return request({
            url: `${api_name}/addComment`,
            method: `post`,
            data: Pgroup
        })
    },
    //添加
    addContent(contentSet) {
        return request({
            url: `${api_name}/addContent`,
            method: 'post',
            data: contentSet
        })
    },
    //获取自己发的信息
    listContent(contentSet) {
        return request({
            url: `${api_name}/ownContents`,
            method: 'post',
            data: contentSet
        })
    },
}